package jwt

import (
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var jwtSecret []byte

// Init the module
func Init() {
	jwtSecretString := os.Getenv("JWT_SECRET")
	jwtSecret = []byte(jwtSecretString)
}

// Create produces base64-encoded jwt token for the given client
func Create(clientID int) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": clientID,
		"iat": time.Now().Unix(),
	})
	return token.SignedString(jwtSecret)
}

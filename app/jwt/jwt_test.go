package jwt

import (
	"reflect"
	"testing"
)

func TestCreate(t *testing.T) {
	Init()
	token, err := Create(42)
	if err != nil {
		t.Error("Unexpected error", err)
	}
	if reflect.TypeOf(token).Kind() != reflect.String {
		t.Error("Wrong token type", token)
	}
}

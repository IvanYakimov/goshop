package controller

import (
	"fmt"
	"log"
	"net/http"

	"golang.org/x/crypto/bcrypt"

	model "bitbucket.org/goshop/app/client/model"
	jwt "bitbucket.org/goshop/app/jwt"

	util "bitbucket.org/goshop/app/util"
)

// Login the client into the system
func Login(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")
	password := r.URL.Query().Get("password")
	cli, err := model.FindOne(email)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if cli == nil {
		fmt.Fprintf(w, "Client not found")
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err := bcrypt.CompareHashAndPassword([]byte(cli.Password), []byte(password)); err != nil {
		fmt.Fprintf(w, "Bad password")
		w.WriteHeader(http.StatusForbidden)
		return
	}
	token, err := jwt.Create(cli.ID)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	response := struct{ AccessToken string }{token}
	fmt.Fprintf(w, util.PrepareResponse(response))
}

package controller

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/dgrijalva/jwt-go"

	model "bitbucket.org/goshop/app/client/model"
)

var jwtSecret []byte

// Init the module
func Init() {
	jwtSecretString := os.Getenv("JWT_SECRET")
	jwtSecret = []byte(jwtSecretString)
}

// Info returns client's info
func Info(w http.ResponseWriter, r *http.Request) {
	var err error
	var cli model.Client
	// TODO: move to a middleware!
	auth := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")
	token, err := jwt.Parse(auth, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return jwtSecret, nil
	})
	var id string
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		idAsNumber := int(claims["sub"].(float64))
		id = strconv.Itoa(idAsNumber)
	} else {
		log.Println(err)
		w.WriteHeader(http.StatusForbidden)
		return
	}
	log.Println("Looking for a client", id)
	cli, err = model.GetOne(id)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		fmt.Fprintf(w, cli.Stringify())
	}
}

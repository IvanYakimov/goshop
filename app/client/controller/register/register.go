package controller

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"golang.org/x/crypto/bcrypt"

	model "bitbucket.org/goshop/app/client/model"
)

var jwtSecret []byte

// Init the module
func Init() {
	jwtSecretString := os.Getenv("JWT_SECRET")
	jwtSecret = []byte(jwtSecretString)
}

// Register a new client
func Register(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")
	password := r.URL.Query().Get("password")
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	cli, err := model.FindOne(email)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if cli != nil {
		fmt.Fprintf(w, "Client with given email is already registered")
		w.WriteHeader(http.StatusConflict)
		return
	}
	fmt.Println("Going to register a new client", email, string(hash))
	err = model.Register(email, string(hash))
	if err != nil {
		log.Println(err)
		fmt.Fprintf(w, "Couldn't register the client")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

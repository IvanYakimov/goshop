package model

import (
	"encoding/json"
	"log"
	"os"

	"github.com/jmoiron/sqlx"
)

var conn *sqlx.DB

// Client info
type Client struct {
	ID       int    `db:"ID"`
	EMail    string `db:"EMail"`
	Password string `db:"Password"`
}

// Init the connection
func Init() {
	user := os.Getenv("MYSQL_USER")
	auth := os.Getenv("MYSQL_AUTH")
	url := os.Getenv("MYSQL_URL")

	conn = sqlx.MustConnect("mysql", user+":"+auth+"@tcp("+url+")/marketplace")
}

// Stringify the client info to be useful in the response
func (c Client) Stringify() string {
	res, err := json.Marshal(c)
	if err != nil {
		return ""
	}
	return string(res)
}

// FindOne client by email
func FindOne(email string) (cli *Client, err error) {
	var clients []Client
	err = conn.Select(&clients, "SELECT * FROM Clients WHERE Email=? LIMIT 1", email)
	if err != nil {
		return
	}
	if len(clients) == 0 {
		return nil, nil
	}
	return &clients[0], nil
}

// Register new client
func Register(email, password string) (err error) {
	var stmt *sqlx.Stmt
	stmt, err = conn.Preparex("INSERT INTO Clients (EMail, Password) VALUES (?, ?)")
	if err != nil {
		return
	}
	log.Println(stmt)
	_, err = stmt.Exec(email, password)
	return
}

// GetOne client by id
func GetOne(id string) (client Client, err error) {
	err = conn.Get(&client, "SELECT * FROM Clients WHERE id=?", id)
	return
}

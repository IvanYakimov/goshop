package util

import (
	"encoding/json"
	"log"
)

// PrepareResponse transforms r into a json
func PrepareResponse(r interface{}) string {
	v, err := json.Marshal(r)
	if err != nil {
		log.Panic("Failed to Marshal response")
	}
	return string(v)
}

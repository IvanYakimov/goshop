package main

import (
	"log"
	"net/http"

	info "bitbucket.org/goshop/app/client/controller/info"
	login "bitbucket.org/goshop/app/client/controller/login"
	register "bitbucket.org/goshop/app/client/controller/register"
	model "bitbucket.org/goshop/app/client/model"
	jwt "bitbucket.org/goshop/app/jwt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func init() {
	model.Init()
	info.Init()
	// login.Init()
	register.Init()
	jwt.Init()
	log.Println("Initialized")
}

func main() {
	log.Println("Starting server")
	r := mux.NewRouter()
	r.Handle("/client/", http.HandlerFunc(info.Info)).Methods("GET")
	r.Handle("/client/login/", http.HandlerFunc(login.Login)).Methods("GET")
	r.Handle("/client/register/", http.HandlerFunc(register.Register)).Methods("POST")
	log.Fatal(http.ListenAndServe(":8080", r))
}
